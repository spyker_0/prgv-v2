/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import bean.Autor;
import fabricaDeSessoes.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author spyker
 */
public class AutorDAO {

    static SessionFactory fabrica = HibernateUtil.getSessionFactory();

    public void adicionarAutor(String nome) {
        Autor autor = new Autor();

        autor.setName(nome);

        Session session = fabrica.openSession();
        session.beginTransaction();

        session.save(autor);

        session.getTransaction().commit();
        session.close();
    }

    public Autor buscarAutor(String nome) {
        Session session = fabrica.openSession();

        Query q = session.createQuery("from Autor where name = :autor");

        Autor autor = (Autor) q.setString("autor", nome).uniqueResult();
        
        return autor;
    }
}
