/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import bean.Autor;
import bean.Livro;
import fabricaDeSessoes.HibernateUtil;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author spyker
 */
public class LivroDAO {

    static SessionFactory fabrica = HibernateUtil.getSessionFactory();

    public void adicionarLivro(List<String> autores, String titulo, int edicao, String isbn) {
        Livro livro = new Livro();

        AutorDAO aDAO = new AutorDAO();

        List<Autor> listaAutores = new ArrayList<>();

        for (String nome : autores) {
            listaAutores.add(aDAO.buscarAutor(nome));
        }

        livro.setAutor(listaAutores);
        livro.setEdicao(edicao);
        livro.setTitulo(titulo);
        livro.setIsbn(isbn);

        Session session = fabrica.openSession();
        session.beginTransaction();

        session.save(livro);

        session.getTransaction().commit();
        session.close();
    }
}
