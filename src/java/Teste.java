
import DAO.AutorDAO;
import DAO.LivroDAO;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author spyker
 */
public class Teste {

    public static void main(String[] args) {
        AutorDAO aDAO = new AutorDAO();
        
        aDAO.adicionarAutor("João");
        aDAO.adicionarAutor("Miai");
        aDAO.adicionarAutor("pdf");
        aDAO.adicionarAutor("Brilhanto");
        
        LivroDAO lDAO = new LivroDAO();
        
        List<String> nomeDeAutores = new ArrayList<String>();
        
        nomeDeAutores.add("João");
        nomeDeAutores.add("Miai");
        
        lDAO.adicionarLivro(nomeDeAutores, "titulo", 1, "bla");
    }
}
